# frwrd

A basic URL shortening Web service (MVP).

For the sake of minimising the technical scope and time spent on this
MVP various aspects are not yet implemented, preventing this from being
considered production-ready. These features include:

- hardening of any sort, such as CSRF, CORS, DDoS prevention, etc.
- performance enhancement of any sort, such as caching, code
  minimisation, etc.
- error-handling
- special header tags

A few interesting notes: I decided to write this without the use of any
frameworks (choosing instead to work with vanilla JS) nor any
traditional RDBS (choosing instead a local FS store, _LevelDB_).

You can also use `curl` et al to interact with the service, using either
`JSON` or `form-urlencoded` payloads:

```bash
curl -i -X POST -H 'content-type: application/json' -d \
  '{"url":"http://foo.baz"}' -w '\n' http://localhost:8000/
```

or

```bash
curl -i -X POST -d 'url=http://foo.baz' -w '\n' http://localhost:8000/
```

## Install

After cloning the repository locally, simply install the dependencies
via _npm_, `npm install`, or using _make_, `make setup`.

## Usage

In your local directory containing the cloned project, run `npm start`
to run the project using _npm_ directly or with _make_ via `make
server`.

## Tests

To run the test suite, run via _npm_, `npm install`, or using _make_, 
`make test`.
