let form

document.addEventListener('DOMContentLoaded', (ev) => {
  form = document.querySelector('form')
  handleFormSubmit()
  handleResetView()
})

function handleResetView() {
  const target = document.querySelector('main')
  return target.addEventListener('click', (ev) => {
    const { tagName } = ev.target
    if (tagName === 'A' && ev.target.getAttribute('href') === '/') {
      ev.preventDefault()
      return target.replaceChildren(form)
    }
  })
}

function handleFormSubmit () {
  return document.body.addEventListener('submit', (ev) => {
    if (ev.target.tagName === 'FORM') {
      ev.preventDefault()
      const input = ev.target.querySelector('input')
      const url = input.value
      input.value = ''
      return shortenUrl(url).then(renderUrl.bind(null, 'main'))
    }
  })
}

function renderUrl(target, url) {
  // Typically, I would approach this with SSR of a shared JS function
  const parent = document.querySelector(target)
  const p1 = document.createElement('p')
  p1.textContent = 'Generated shortened URL: '
  const urlLink = document.createElement('a')
  urlLink.setAttribute('href', url)
  urlLink.textContent = url
  p1.appendChild(urlLink)
  const p2 = document.createElement('p')
  const homeLink = document.createElement('a')
  homeLink.setAttribute('href', '/')
  homeLink.textContent = 'Create another short URL'
  p2.appendChild(homeLink)
  return parent.replaceChildren(p1, p2)
}

function shortenUrl(url) {
  return fetch('/', {
    method: 'POST',
    body: JSON.stringify({ url }),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  })
    .then(res => {
      let json = res.text()
      if (res.status >= 200 && res.status < 300) return json
      return json.then(Promise.reject.bind(Promise))
    })
}
