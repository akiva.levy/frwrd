const { F_OK } = require('fs').constants
const fs = require('fs').promises
const { resolve } = require('path')
const lib = resolve(__dirname, '../lib/')
const symlinkPath = resolve(__dirname, '../node_modules/_')

fs.access(symlinkPath, F_OK)
  .catch(err => fs.symlink(lib, symlinkPath))
  .then(() => console.log('./lib successfully symlinked as ./node_modules/_'))

