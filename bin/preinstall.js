const fs = require('fs').promises
const { resolve, join } = require('path')
const cp = require('child_process')
const lib = resolve(__dirname, '../lib/')

function installDependency (cwd) {
  return new Promise(function (resolve, reject) {
    return fs.access(join(cwd, 'package.json'))
      .then(() =>
        cp.spawn('npm', ['i'], { env: process.env, cwd, stdio: 'inherit' })
      )
      .catch(() => {})
  })
}

fs.readdir(lib)
  .then(mods => Promise.all(mods.map(m => installDependency(join(lib, m)))))
