process.title = require('./package.json').name

const argv = require('minimist')(process.argv.slice(2), {
  alias: { p: 'port' },
  default: { p: 8000 },
})

const db = require('_/db')
const server = require('_/server')(db)

server.listen(process.env.PORT || argv.port, () =>
  console.log(`Server running at http://localhost:${server.address().port}`)
)
