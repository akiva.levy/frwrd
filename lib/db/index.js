const { resolve } = require('path')
const levelup = require('levelup')
const leveldown = require('leveldown')

module.exports = levelup(leveldown(resolve('db')))
