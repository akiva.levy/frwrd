const { createReadStream } = require('fs')
const { resolve } = require('path')
const trumpet = require('trumpet')
const { pipeline } = require('stream')

module.exports = (view, res) => {
  const tr = trumpet()
  const ws = tr.select('main').createWriteStream()
  if (/\.html$/i.test(view))
    createReadStream(view).pipe(ws)
  else
    ws.end(view)
  res.writeHead(200, { 'Content-Type': 'text/html' })
  return pipeline(
    createReadStream('./assets/layout.html'),
    tr,
    res,
    err => err && process.stderr.write(`HTML pipeline failed:\n${err}\n`)
  )
}
