const http = require('http')
const { createReadStream } = require('fs')
const querystring = require('querystring')

module.exports = (db) => http.createServer((req, res) => {
  req.body = req.body || {}
  req.db = db
  req.on('data', chunk => {
    try {
      const { 'content-type': contentType } = req.headers
      if (contentType === 'application/x-www-form-urlencoded')
        body = querystring.parse(chunk.toString())
      if (/application\/json/.test(contentType))
        body = JSON.parse(chunk.toString())
      req.body = Object.assign({}, req.body, body)
    }
    catch (e) {
      // TODO handle error
      throw e
    }
  })
  req.on('end', () => {
    if (/^\/main\.css$/.test(req.url))
      return createReadStream('./assets/main.css').pipe(res)
    if (/^\/main\.js$/.test(req.url))
      return createReadStream('./assets/main.js').pipe(res)
    if (req.url === '/favicon.ico')
      return res.statusCode = 204 && res.end()
    return require('_/slug/routes')(req, res)
  })
})
