const yup = require('yup')
const { nanoid } = require('nanoid')

const schema = yup.object().shape({
  id: yup.string().default(nanoid),
  url: yup.string().url().required(),
  createdOn: yup.number().default(Date.now),
})

module.exports = schema
