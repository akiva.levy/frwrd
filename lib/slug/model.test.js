const test = require('tape')
const model = require('./model')

test('Slug model', function (t) {
  t.test('has properties ', function (st) {
    const props = Object.keys(model.describe().fields)
    st.equal(props.length, 3)
    st.deepEqual(props, ['id', 'url', 'createdOn'])
    st.end()
  })

  t.test('requires a valid URL', function (st) {
    st.plan(2)
    model.validate({})
      .catch(e => st.equal(e.message, 'url is a required field'))
    model.validate({ url: 'http://foo.xyz' })
      .then(m => st.ok(m))
  })

  t.test('returns an object', function (st) {
    st.plan(2)
    model.validate({ url: 'http://foo.xyz' })
      .then(m => {
        st.equal(typeof m === 'object', true)
        st.equal(m !== null, true)
      })
  })

  t.test('returned object', function (st) {

    st.test('contains a createdOn property', function (st) {
      st.plan(2)
      model.validate({ url: 'http://foo.xyz' })
        .then(m => {
          st.ok(m.createdOn)
          st.equal(typeof m.createdOn, 'number')
        })
    })

    st.test('contains a url property', function (st) {
      st.plan(1)
      model.validate({ url: 'http://foo.xyz' })
        .then(m => st.equal(m.url, 'http://foo.xyz'))
    })

    st.test('contains an ID property', function (st) {
      st.plan(2)
      model.validate({ url: 'http://foo.xyz' })
        .then(m => {
          st.equal(typeof m.id, 'string')
          st.equal(m.id.length, 21)
        })
    })

    st.end()
  })

  t.end()
})
