const { model: slug, service } = require('.')
const render = require('_/render')

module.exports = (req, res) => {
  const srv = service(req.db)

  if (req.url === '/' && req.method === 'GET')
    return render('./assets/form.html', res)

  if (/^\/([0-9a-z_-]){21}$/i.test(req.url)) {
    if (req.method === 'GET')
      return srv.get(req.url.substring(1), { asBuffer: false })
        .then(slug => {
          slug = JSON.parse(slug)
          res.writeHead(302, { 'Location': slug.url })
          return res.end()
        })
        .catch(err => res.end(err.message))
  }

  if (req.url === '/' && req.method === 'POST') {
    // TODO Implement interface
    return slug.validate({ url: req.body.url.trim() })
      .then(s => srv.add(s).then(() => s))
      .then(s => {
        const { 'content-type': contentType, accept } = req.headers
        const { host } = req.headers
        const url = `http://${host}/${s.id}`
        const format = accept.split(',')[0]
        if (contentType === 'application/json' || format !== 'text/html')
          return res.writeHead(200, {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(url),
          })
            .end(url)
        if (contentType === 'application/x-www-form-urlencoded')
          return render(`
            <p>Generated shortened URL: <a href="${url}">${url}</a></p>
            <p><a href="/">Create another short URL</a></p>
          `, res)
      })
      .catch(err => res.end(err.message))
  }

  return res.statusCode = 500 && res.end(req.url + ' not found!')
}
