module.exports = function (db) {
  if (!db) throw Error('A database connection is required')
  return {
    add: (data, cb) => {
      if (!data || typeof data !== 'object')
        throw new Error('A slug is required')
      return db.put(data.id, JSON.stringify(data), cb)
    },
    get: (id, cb) => {
      if (!id) throw Error('An ID is required')
      return db.get(id, cb)
    },
    remove: (id, cb) => {
      if (!id) throw Error('An ID is required')
      return db.del(id, cb)
    },
  }
}
