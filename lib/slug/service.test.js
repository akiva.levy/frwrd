const test = require('tape')
const { nanoid } = require('nanoid')
const service = require('./service')
const { resolve } = require('path')
const levelup = require('levelup')
const leveldown = require('leveldown')
const fs = require('fs')

let db, srv
fakeSlug = () => ({
  createdOn: Date.now(),
  url: 'foo.com',
  id: nanoid()
})

test('Slug service', function (t) {
  t.test('setup', function (st) {
    // TODO Mock DB instead
    db = levelup(leveldown(resolve(__dirname, 'test.db')))
    srv = service(db)
    st.end()
  })

  t.test('is a factory function', function (st) {
    st.equal(typeof service, 'function')
    st.end()
  })

  t.test('requires a database', function (st) {
    st.throws(service, /database connection is required/)
    st.ok(service({}))
    st.end()
  })

  t.test('returns service methods', function (st) {
    const srv = service({})
    st.equal(Object.keys(srv).length, 3)
    st.equal(typeof srv.add, 'function')
    st.equal(typeof srv.get, 'function')
    st.equal(typeof srv.remove, 'function')
    st.end()
  })

  t.test('#add', function (st) {
    st.test('requires a slug', function (st) {
      st.throws(srv.add, /slug is required/i)
      st.doesNotThrow(() => srv.add(fakeSlug()), /slug is required/i)
      st.end()
    })
    st.test('accepts a callback if provided', function (st) {
      st.plan(1)
      srv.add(fakeSlug(), () => st.ok(1))
    })
    st.test('returns a promise if no callback is provided', function (st) {
      st.plan(1)
      srv.add(fakeSlug()).then(() => st.ok(1))
    })
    st.test('saves the new slug', function (st) {
      st.plan(1)
      srv.add(fakeSlug())
        .then(res => st.ok(1))
    })
  })

  t.test('#get', function (st) {
    const slug = fakeSlug()
    srv.add(slug)
      .then(() => {
        st.test('requires an ID', function (st) {
          st.throws(srv.get, /id is required/i)
          st.doesNotThrow(() => srv.get(slug.id), /id is required/i)
          st.end()
        })
        st.test('accepts a callback if provided', function (st) {
          st.plan(1)
          srv.get(slug.id, () => st.ok(true))
        })
        st.test('returns a promise if no callback is provided', function (st) {
          st.plan(1)
          srv.get(slug.id).then(() => st.ok(1))
        })
      })
  })

  t.test('#remove', function (st) {
    st.test('requires an ID', function (st) {
      st.throws(srv.remove, /id is required/i)
      st.doesNotThrow(() => srv.remove('foo'), /id is required/i)
      st.end()
    })
    st.test('accepts a callback if provided', function (st) {
      st.plan(1)
      srv.remove('foo', () => st.ok(true))
    })
    st.test('returns a promise if no callback is provided', function (st) {
      st.plan(1)
      srv.remove(fakeSlug().id).then(() => st.ok(1))
    })
    st.test('removed the entry from the db', function (st) {
      st.plan(1)
      const slug = fakeSlug()
      srv.add(slug)
        .then(() => srv.remove(slug.id))
        .then(() => srv.get(slug.id))
        .catch(err => st.ok(/not found/.test(err)))
    })
  })

  t.test('teardown', function (st) {
    fs.rm(
      resolve(__dirname, 'test.db'),
      { force: true, recursive: true },
      st.end
    )
  })
})
