.PHONY: $(MAKECMDGOALS)

.PHONY: setup
setup:
	npm i

.PHONY: server
server:
	npm start

.PHONY: test
test:
	npm t
